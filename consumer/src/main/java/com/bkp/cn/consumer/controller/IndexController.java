package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.*;
import com.bkp.cn.common.service.*;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "首页")
@Log4j
@RestController
@RequestMapping(value = "/index")
public class IndexController {
    @Reference
    private LeftMenusService leftMenusService;
    @Reference
    private AdvertisementService advertisementService;
    @Reference
    private ImgService imgService;
    @Reference
    private CourseService courseService;
    @Reference
    private ActivityService activityService;
    @Reference
    private ArticleService articleService;
    @Reference
    private MenusService menusService;


    @ApiOperation(value = "首页查询")
    @GetMapping(value = "/select")
    public ReturnResult<Map<String,Object>> select() {
        Map<String, Object> maps = Maps.newHashMap();
        List<LeftMenu> leftMenuList = leftMenusService.selectLeftMenus(1, 2, 3, 4, 5);
        List<Img> imgList = imgService.selectImgRand();
        List<Advertisement> advertisements = advertisementService.selectAdvWeightRandom();
        List<Course> courseList = courseService.selectCourseRand();
        List<Activity> activityList = activityService.selectActivityRand();
        List<Menu> menuList = menusService.selectMenus();
        Map articleMap = articleService.selectArticlesBySearch(null, 1, 1, 5);
        maps.put("leftMenuList", leftMenuList);
        maps.put("advertisements", advertisements);
        maps.put("imgList", imgList);
        maps.put("courseList", courseList);
        maps.put("activityList", activityList);
        maps.put("articleList", articleMap.get("currentList"));
        maps.put("menuList", menuList);
        return ReturnResultUtils.returnSucess(maps);
    }
}
