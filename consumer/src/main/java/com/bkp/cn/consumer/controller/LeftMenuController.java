package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.LeftMenu;
import com.bkp.cn.common.service.LeftMenusService;
import com.bkp.cn.common.vo.LeftMenuVo;
import com.bkp.cn.common.vo.MenuVo;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@Api(tags = "左侧菜单栏")
@RestController
@RequestMapping(value = "/leftMenu")
public class LeftMenuController {
    @Reference
    private LeftMenusService leftMenusService;

    @ApiOperation(value = "选择查询左侧菜单栏")
    @GetMapping(value = "/selectLeftMenus")
    public ReturnResult<List<LeftMenu>> selectLeftMenus(@RequestParam(name = "ids", required = false, defaultValue = "2,3,4") Integer... ids) {
        return ReturnResultUtils.returnSucess(leftMenusService.selectLeftMenus(ids));
    }

    @ApiOperation(value = "后台添加左侧菜单栏")
    @PostMapping(value = "/insertLeftMenus")
    public ReturnResult insertLeftMenus(@RequestParam(name = "leftMenuName") String leftMenuName) {
        if (CollectionUtils.isEmpty(leftMenusService.selectLeftMenus(leftMenuName))) {
            if (leftMenusService.insertLeftMenus(leftMenuName)) return ReturnResultUtils.returnSucess();

            return ReturnResultUtils.returnFail(333, "添加失败");
        }
        return ReturnResultUtils.returnFail(606, "添加失败,请勿重复添加");
    }

}
