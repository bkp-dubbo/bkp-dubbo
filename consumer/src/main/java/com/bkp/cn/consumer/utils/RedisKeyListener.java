package com.bkp.cn.consumer.utils;

import lombok.extern.log4j.Log4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

@Log4j
@Component
public class RedisKeyListener extends KeyExpirationEventMessageListener {
    /**
     * 监听redis失效
     *
     * @param listenerContainer
     */
    public RedisKeyListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /**
     * 获取失效信息
     *
     * @param message
     * @param pattern
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        super.onMessage(message, pattern);
        log.info("订单编号：" + message);
    }
}
