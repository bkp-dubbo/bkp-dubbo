package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.conf.WxConfig;
import com.bkp.cn.common.dto.Wx;
import com.bkp.cn.common.service.UserService;
import com.bkp.cn.common.service.WxService;
import com.bkp.cn.common.utils.ActiveMqUtils;
import com.bkp.cn.common.utils.HttpClientUtils;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.common.vo.WxVo;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/210:10
 */
@Api(tags = "微信")
@Controller
@RequestMapping(value = "/wx")
public class WxController {
    @Autowired
    private WxConfig wxConfig;
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private WxService wxService;
    @Reference
    private UserService userService;
    @Autowired
    private ActiveMqUtils activeMqUtils;
    String login_info = "login_info:";

    @ApiOperation(value = "微信登录")
    @GetMapping(value = "/wxLogin")
    public String wxLogin() {
        String codeUri = wxConfig.reqCodeUri();
        return "redirect:" + codeUri;
    }

    @GetMapping(value = "/callBack")
    public String callBack(String code) throws IOException {
        if (null != code && code != "") {
            String accessTokenStr = HttpClientUtils.doGet(wxConfig.reqAccessTokenUrl(code));
            JSONObject jsonObject = JSONObject.parseObject(accessTokenStr);
            String accessToken = jsonObject.getString("access_token");
            String openId = jsonObject.getString("openid");
            String userInfo = HttpClientUtils.doGet(wxConfig.reqUserInfoUrl(accessToken, openId));
            //微信登陆后将信息存入数据库
            WxVo wxVo = JSONObject.parseObject(userInfo, WxVo.class);
            activeMqUtils.sendQueueMsg("wxLogin.wxVo", wxVo);
            return "redirect:" + wxConfig.getLoginSuccess() + openId;
        }
        return "redirect:" + wxConfig.getLoginFail();
    }

    @ApiOperation(value = "绑定手机")
    @GetMapping(value = "/bindTel")
    @ResponseBody
    public ReturnResult bindTel(@RequestParam String tel, @RequestParam String openId) {
        if (!wxService.isBindTel(openId)) {
            if (wxService.bindTel(tel, openId)) {
                UserVo userVo = userService.selectUserByTel(tel);
                try {
                    redisUtils.set(login_info + openId, JSONObject.toJSONString(userVo));
                    return ReturnResultUtils.returnSucess(openId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return ReturnResultUtils.returnFail(311, "绑定手机失败！！！");
        }
        return ReturnResultUtils.returnSucess(312, "手机已绑定！！！", openId);
    }

    @ApiOperation(value = "通过openId查询微信信息")
    @GetMapping("/selectWxByOpenId")
    @LoginReqired
    @ResponseBody
    public ReturnResult selectWxByOpenId(@RequestParam String openId) {
        return ReturnResultUtils.returnSucess(wxService.selectWxByOpenId(openId));
    }
}