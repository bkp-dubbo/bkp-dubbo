package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Article;
import com.bkp.cn.common.dto.ArticleLike;
import com.bkp.cn.common.enums.ErrorEnums;
import com.bkp.cn.common.exception.ServiceException;
import com.bkp.cn.common.service.ArticleService;
import com.bkp.cn.common.utils.PageUtils;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.vo.ArticleVo;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.consumer.conf.CurrentUser;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Api(tags = "文章")
@Configuration
@RestController
@RequestMapping(value = "/article")
@EnableScheduling
public class ArticleController {
    @Reference
    private ArticleService articleService;
    @Autowired
    private RedisUtils redisUtils;
    String article_like = "article_like:";

    @ApiOperation(value = "高级查询文章")
    @GetMapping(value = "/selectArticles")
    public ReturnResult<PageUtils<List<Article>>> selectArticlesBySearch(@RequestParam(name = "searchStr", required = false) String searchStr,
                                               @RequestParam(name = "leftMenuId", required = false) Integer leftMenuId,
                                               @RequestParam(name = "pageNo", required = false, defaultValue = "1") int pageNo,
                                               @RequestParam(name = "pageSize", required = false, defaultValue = "10") int pageSize) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setCurrentPage(pageNo);
        Map<String, Object> articleMap = articleService.selectArticlesBySearch(searchStr, leftMenuId, pageUtils.getPageNo(), pageSize);
        pageUtils.setCurrentList((List<Article>) articleMap.get("currentList"));
        pageUtils.setTotalCount((long) articleMap.get("totalCount"));
        return ReturnResultUtils.returnSucess(pageUtils);
    }

    @ApiOperation(value = "查询文章详情")
    @GetMapping(value = "/selectArticleById")
    public ReturnResult<Article> selectArticleById(@RequestParam Integer id) {
        return ReturnResultUtils.returnSucess(articleService.selectArticleById(id));
    }

    @ApiOperation(value = "添加文章")
    @LoginReqired
    @PostMapping(value = "/insertArticle")
    public ReturnResult insertArticle(@CurrentUser UserVo userVo, @Validated ArticleVo articleVo) {
        articleVo.setUserId(userVo.getId());
        if (articleService.insertArticle(articleVo)) return ReturnResultUtils.returnSucess();
        return ReturnResultUtils.returnFail(201, "添加失败！");
    }

    @ApiOperation(value = "点赞")
    @LoginReqired
    @GetMapping(value = "/like")
    public ReturnResult like(@CurrentUser UserVo userVo, @RequestParam Integer articleId, @RequestParam Integer count) {
        if (!redisUtils.checkFreq(article_like + userVo.getId(), 2, 5)) {
            return ReturnResultUtils.returnFail(21, "点赞太频繁了");
        }
        articleService.like(userVo.getId(), articleId, count);
        return ReturnResultUtils.returnSucess();
    }

    @ApiOperation(value = "修改文章")
    @LoginReqired
    @PostMapping(value = "/updateArticle")
    public ReturnResult updateArticle(@CurrentUser UserVo userVo, @Validated ArticleVo articlevo) {
        articlevo.setUserId(userVo.getId());
        if (articleService.updateArticle(articlevo)) return ReturnResultUtils.returnSucess();
        return ReturnResultUtils.returnFail(201, "修改失败！");
    }

    @ApiOperation(value = "删除文章")
    @LoginReqired
    @GetMapping(value = "/delArticle")
    public ReturnResult delArticle(@RequestParam(name = "articleId") Integer articleId) {
        if (articleService.delArticle(articleId)) return ReturnResultUtils.returnSucess();
        return ReturnResultUtils.returnFail(201, "删除失败！");
    }

}
