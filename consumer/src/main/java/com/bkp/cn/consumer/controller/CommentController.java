package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Comment;
import com.bkp.cn.common.service.CommentService;
import com.bkp.cn.common.vo.CommentVo;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.consumer.conf.CurrentUser;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "评论")
@RestController
@RequestMapping(value = "/comment")
public class CommentController {
    @Reference
    private CommentService commentService;

    @ApiOperation(value = "添加评论")
    @LoginReqired
    @PostMapping(value = "/insertComment")
    public ReturnResult insertComment(@CurrentUser UserVo userVo, @Validated CommentVo commentVo) {
        commentVo.setUserId(userVo.getId());
        if (commentService.insertComment(commentVo)) return ReturnResultUtils.returnSucess();

        else return ReturnResultUtils.returnFail(333, "添加失败");
    }

    @ApiOperation(value = "修改评论")
    @LoginReqired
    @PostMapping(value = "/updateComment")
    public ReturnResult updateComment(@CurrentUser UserVo userVo, @Validated CommentVo commentVo) {
        commentVo.setUserId(userVo.getId());
        if (commentService.updateComment(commentVo)) return ReturnResultUtils.returnSucess();

        return ReturnResultUtils.returnFail(444, "修改失败");
    }

    @ApiOperation(value = "删除评论")
    @LoginReqired
    @GetMapping(value = "/deleteComment")
    public ReturnResult deleteComment(@RequestParam(name = "id") int id) {
        if (commentService.deleteComment(id)) return ReturnResultUtils.returnSucess();

        return ReturnResultUtils.returnFail(111, "删除失败");
    }

    @ApiOperation(value = "查询评论")
    @GetMapping(value = "/selectComment")
    public ReturnResult<List<Comment>> selectComment(@RequestParam(name = "articleId") Integer articleId) {
        List<Comment> commentList = commentService.selectComment(articleId);
        if (CollectionUtils.isNotEmpty(commentList))
            return ReturnResultUtils.returnSucess(commentList);

        return ReturnResultUtils.returnFail(999, "暂无评论");
    }
}