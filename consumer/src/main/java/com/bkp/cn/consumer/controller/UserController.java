package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.service.UserService;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.utils.SHAUtils;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/4/3010:21
 */
@Api(tags = "用户")
@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Reference
    private UserService userService;
    @Autowired
    private RedisUtils redisUtils;
    String login_info = "login_info:";

    @ApiOperation(value = "注册")
    @PostMapping(value = "/register")
    public ReturnResult register(@ApiParam(value = "手机号", required = true) @RequestParam String tel,
                                 @ApiParam(value = "密码", required = true) @RequestParam String password,
                                 @ApiParam(value = "用户名", required = true) @RequestParam String username) {
        //判断用户是否已注册
        if (ObjectUtils.isEmpty(userService.selectUserByTel(tel))) {
            UserVo userVo = new UserVo();
            userVo.setUsername(username);
            userVo.setTel(tel);
            userVo.setPassword(password);
            if (userService.register(userVo)) return ReturnResultUtils.returnSucess();

            return ReturnResultUtils.returnFail(2, "注册失败");
        }
        return ReturnResultUtils.returnFail(1, "手机号已注册！");
    }

    @ApiOperation(value = "手机号登录")
    @PostMapping(value = "/telLogin")
    public ReturnResult telLogin(@RequestParam String tel, @RequestParam(name = "password", required = false) String password, HttpServletRequest request) {
        UserVo userVo = userService.selectUserByTel(tel);
        if (!ObjectUtils.isEmpty(userVo)) {
            String token = request.getSession().getId();
            //用户密码为空走也验证码，不为空走账户密码登录
            if (StringUtils.isNotEmpty(password)) {
                String pwd = SHAUtils.stringSha1(password);
                if (pwd.equals(userVo.getPassword())) {
                    try {
                        redisUtils.set(login_info + token, JSONObject.toJSONString(userVo));
                        return ReturnResultUtils.returnSucess(token);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return ReturnResultUtils.returnFail(22, "缓存异常！");
                    }
                }
            } else {
                try {
                    redisUtils.set(login_info + token, JSONObject.toJSONString(userVo));
                    return ReturnResultUtils.returnSucess(token);
                } catch (Exception e) {
                    e.printStackTrace();
                    return ReturnResultUtils.returnFail(22, "缓存异常！");
                }
            }
        }
        return ReturnResultUtils.returnFail(21, "无法找到匹配的用户！");
    }

    @ApiOperation(value = "退出登录")
    @LoginReqired
    @GetMapping(value = "/exit")
    public ReturnResult exit(HttpServletRequest request) {
        String token = request.getHeader("token");
        try {
            redisUtils.del(login_info + token);
            return ReturnResultUtils.returnSucess();
        } catch (Exception e) {
            e.printStackTrace();
            return ReturnResultUtils.returnFail(22, "缓存异常！");
        }
    }

}
