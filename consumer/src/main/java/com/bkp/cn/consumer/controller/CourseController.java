package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.conf.WxPayConfig;
import com.bkp.cn.common.dto.Course;
import com.bkp.cn.common.dto.CourseMember;
import com.bkp.cn.common.dto.Orders;
import com.bkp.cn.common.service.CourseService;
import com.bkp.cn.common.service.WxPayService;
import com.bkp.cn.common.utils.PageUtils;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.utils.WxPayUtils;
import com.bkp.cn.common.vo.CourseVo;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.consumer.conf.CurrentUser;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

@Api(tags = "课程")
@Log4j
@RestController
@RequestMapping(value = "/course")
public class CourseController {
    @Reference
    private CourseService courseService;
    @Reference
    private WxPayService wxPayService;

    @ApiOperation(value = "后台添加课程内容")
    @PostMapping(value = "/insertCourse")
    public ReturnResult insertCourse(@Validated CourseVo courseVo) {
        if (CollectionUtils.isEmpty(courseService.selectByCourseName(courseVo.getCourseName()))) {
            if (courseService.insertCourse(courseVo)) return ReturnResultUtils.returnSucess();

            return ReturnResultUtils.returnFail(201, "添加失败！");
        }
        return ReturnResultUtils.returnFail(211, "添加失败,请勿重复添加！");
    }

    @ApiOperation(value = "首页课程随机推荐")
    @GetMapping(value = "/selectCourseRand")
    public ReturnResult<List<Course>> selectCourseRand() {
        return ReturnResultUtils.returnSucess(courseService.selectCourseRand());
    }

    @ApiOperation(value = "全部课程+课程名关键字查询")
    @GetMapping(value = "/selectCourse")
    public ReturnResult<PageUtils<List<Course>>> selectCourse(@RequestParam(name = "pageNo", required = true) int pageNo,
                                     @RequestParam(name = "pageSize", required = true) int pageSize,
                                     @RequestParam(name = "selectStr", defaultValue = "") String selectStr) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(pageSize);
        Map<String, Object> maps = courseService.selectCourse(pageUtils.getPageNo(), pageSize, selectStr);
        pageUtils.setCurrentList((List<Course>) maps.get("currentList"));
        pageUtils.setCurrentPage(pageNo);
        pageUtils.setTotalCount((Long) maps.get("totalCount"));
        return ReturnResultUtils.returnSucess(pageUtils);
    }

    @ApiOperation(value = "未登录下单个课程详情")
    @GetMapping(value = "/selectCourseById")
    public ReturnResult<Course> selectCourseById(@RequestParam(name = "id", required = true) Integer id) {
        return ReturnResultUtils.returnSucess(courseService.selectCourseById(id));
    }

    @ApiOperation(value = "查询已有课程,需登录")
    @LoginReqired
    @GetMapping(value = "/selectCourseByUserId")
    public ReturnResult<List<Course>> selectCourseByUserTel(@CurrentUser UserVo userVo, @RequestParam(name = "courseId", required = false) Integer courseId) {
        List<CourseMember> courseMemberList = courseService.selectCourseByUserId(userVo.getTel(), courseId);
        if (CollectionUtils.isNotEmpty(courseMemberList)) return ReturnResultUtils.returnSucess(courseMemberList);

        return ReturnResultUtils.returnFail(7, "对不起，您还未购买任何课程");
    }

    @ApiOperation(value = "购买课程-微信付款")
    @LoginReqired
    @PostMapping(value = "/buyNow")
    public ReturnResult buyNow(@RequestParam(name = "courseId") Integer courseId, @CurrentUser UserVo userVo) {
        return ReturnResultUtils.returnSucess(111, "请扫码付款", wxPayService.isOrder(courseId, userVo.getTel()));
    }
}
