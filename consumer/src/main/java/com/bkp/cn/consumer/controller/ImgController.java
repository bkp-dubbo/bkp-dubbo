package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Article;
import com.bkp.cn.common.dto.Img;
import com.bkp.cn.common.service.ImgService;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "图片")
@RestController
@RequestMapping(value = "/img")
public class ImgController {
    @Reference
    private ImgService imgService;

    @ApiOperation(value = "随机查询图片")
    @GetMapping(value = "/selectImgRand")
    public ReturnResult<List<Img>> selectImgRand() {
        return ReturnResultUtils.returnSucess(imgService.selectImgRand());
    }

    @ApiOperation(value = "查询图片详情")
    @GetMapping(value = "/selectImgById")
    public ReturnResult<Img> selectImgById(@RequestParam(name = "id", required = false) Integer id) {
        return ReturnResultUtils.returnSucess(imgService.selectImgById(id));
    }
}
