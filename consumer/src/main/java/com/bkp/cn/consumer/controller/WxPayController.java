package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.conf.WxPayConfig;
import com.bkp.cn.common.dto.Orders;
import com.bkp.cn.common.service.WxPayService;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.utils.WxPayUtils;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

@Api(tags = "微信支付")
@Controller
@RequestMapping(value = "wxPay")
public class WxPayController {
    @Reference
    private WxPayService wxPayService;
    @Autowired
    private WxPayConfig wxPayConfig;
    @Autowired
    private RedisUtils redisUtils;

    @RequestMapping(value = "/wxPayNotify")
    public String wxPayNotify(HttpServletRequest request) throws Exception {
        InputStream inputStream = request.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuffer sb = new StringBuffer();
        String line;
        while (null != (line = bufferedReader.readLine())) {
            sb.append(line);
        }
        bufferedReader.close();
        inputStream.close();
        Map<String, String> resultMap = WxPayUtils.xmlToMap(sb.toString());
        String wxSign = resultMap.get("sign");
        boolean isCheck = WxPayUtils.isCheckSign(resultMap, wxPayConfig.getKey(), wxSign);
        if (isCheck) {
            Map<String, String> clientMap = Maps.newHashMap();
            clientMap.put("return_code", "SUCCESS");
            clientMap.put("return_msg", "OK");
            String nameSpace = "COURSE_ORDERS:";
            String orderId = resultMap.get("out_trade_no");
            if (redisUtils.hasKey(nameSpace + orderId)) {
                Orders orders = JSONObject.parseObject(redisUtils.get(nameSpace + orderId).toString(), Orders.class);
                wxPayService.WxPaySuccess(orders);
                redisUtils.expire(nameSpace + orderId, 2l);
                return "redirect:" + wxPayConfig.getWxPaySuccess();
            }
            return WxPayUtils.mapToXml(clientMap);
        }
        return null;
    }
}
