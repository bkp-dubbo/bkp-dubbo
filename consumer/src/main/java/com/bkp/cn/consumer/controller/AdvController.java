package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Advertisement;
import com.bkp.cn.common.exception.ServiceException;
import com.bkp.cn.common.service.AdvertisementService;
import com.bkp.cn.common.vo.AdvertisementVo;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/510:47
 */
@Api(tags = "广告")
@RestController
@RequestMapping(value = "/advertisement")
public class AdvController {
    @Reference
    private AdvertisementService advertisementService;

    @ApiOperation(value = "广告推荐")
    @GetMapping(value = "/selectAdvWeightRamdom")
    public ReturnResult<List<Advertisement>> selectAdvWeightRandom() {
        return ReturnResultUtils.returnSucess(advertisementService.selectAdvWeightRandom());
    }

    @ApiOperation(value = "后台添加广告")
    @PostMapping(value = "/insertAdvertisement")
    public ReturnResult insertAdvertisement(@Validated AdvertisementVo advertisementVo) {
        if (CollectionUtils.isEmpty(advertisementService.selectAdvByUrl(advertisementVo.getUrl()))) {
            if (advertisementService.insertAdvertisement(advertisementVo)) return ReturnResultUtils.returnSucess();

            return ReturnResultUtils.returnFail(41, "添加广告失败！！！");
        }
        return ReturnResultUtils.returnFail(44, "广告已存在，不能重复添加！！!");
    }

    @ApiOperation(value = "后台修改广告")
    @PostMapping(value = "/updateAdvertisement")
    public ReturnResult updateAdvertisement(@Validated AdvertisementVo advertisementVo) {
        if (advertisementService.updateAdvertisement(advertisementVo)) return ReturnResultUtils.returnSucess();

        return ReturnResultUtils.returnFail(42, "修改广告失败！！！");
    }

    @ApiOperation(value = "后台删除广告")
    @GetMapping(value = "/delAdvertisement")
    public ReturnResult delAdvertisement(@RequestParam Integer advId) {
        if (advertisementService.delAdvertisement(advId)) return ReturnResultUtils.returnSucess();

        return ReturnResultUtils.returnFail(43, "删除广告失败！！！");
    }

    @ApiOperation(value = "通过id查询广告")
    @GetMapping(value = "/selectAdvById")
    public ReturnResult<Advertisement> selectAdvById(@RequestParam Integer advId) {
        return ReturnResultUtils.returnSucess(advertisementService.selectAdvById(advId));
    }
}
