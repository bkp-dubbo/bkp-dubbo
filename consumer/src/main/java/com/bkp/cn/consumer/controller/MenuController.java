package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Menu;
import com.bkp.cn.common.service.MenusService;
import com.bkp.cn.common.vo.MenuVo;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@Api(tags = "菜单栏")
@RestController
@RequestMapping(value = "/menu")
public class MenuController {
    @Reference
    private MenusService menusService;

    @ApiOperation("查询菜单栏")
    @GetMapping(value = "/selectMenu")
    public ReturnResult<List<Menu>> selectMenu() {
        return ReturnResultUtils.returnSucess(menusService.selectMenus());
    }

    @ApiOperation("查询单个菜单")
    @GetMapping(value = "/selectMenuById")
    public ReturnResult<Menu> selectMenuById(@RequestParam(name = "id") Integer id) {
        return ReturnResultUtils.returnSucess(menusService.selectMenuById(id));
    }
}
