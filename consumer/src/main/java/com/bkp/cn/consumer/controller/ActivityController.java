package com.bkp.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.bkp.cn.common.dto.Activity;
import com.bkp.cn.common.dto.ActivityExample;
import com.bkp.cn.common.dto.Course;
import com.bkp.cn.common.service.ActivityService;
import com.bkp.cn.common.utils.PageUtils;
import com.bkp.cn.common.vo.*;
import com.bkp.cn.consumer.conf.CurrentUser;
import com.bkp.cn.consumer.conf.LoginReqired;
import com.bkp.cn.consumer.utils.ReturnResult;
import com.bkp.cn.consumer.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;

import io.swagger.models.Model;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Api(tags = "活动")
@Log4j
@RestController
@RequestMapping(value = "/activity")
public class ActivityController {
    @Reference
    private ActivityService activityService;

    @ApiOperation(value = "首页活动随机推荐")
    @GetMapping(value = "/selectActivityRand")
    public ReturnResult<List<Activity>> selectActivityRand() {
        return ReturnResultUtils.returnSucess(activityService.selectActivityRand());
    }

    @ApiOperation(value = "高级查询活动")
    @PostMapping(value = "/selectActivityBySearch")
    public ReturnResult<PageUtils<List<Activity>>> selectActivityBySearch(@Validated SearchVo searchVo) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(searchVo.getPageNo());
        pageUtils.setPageSize(searchVo.getPageSize());
        searchVo.setPageNo(pageUtils.getPageNo());
        Map<String, Object> maps = activityService.selectActivityBySearch(searchVo);
        pageUtils.setCurrentList((List<Course>) maps.get("activityList"));
        pageUtils.setCurrentPage(searchVo.getPageNo());
        pageUtils.setTotalCount((Long) maps.get("totalCount"));
        return ReturnResultUtils.returnSucess(pageUtils);
    }

    @ApiOperation(value = "查询活动详情")
    @GetMapping(value = "/selectActivityById")
    public ReturnResult<Activity> selectActivityById(@RequestParam(name = "id", required = true) Integer id) {
        return ReturnResultUtils.returnSucess(activityService.selectActivityById(id));
    }

    @ApiOperation(value = "用户报名参加活动")
    @LoginReqired
    @PostMapping(value = "/insertActivityMember")
    public ReturnResult insertActivityMember(@CurrentUser UserVo userVo, @Validated ActivityMemberVo activityMembervo) {
        activityMembervo.setUserId(userVo.getId());
        if (activityService.insertActivityMember(activityMembervo)) {
            return ReturnResultUtils.returnSucess();
        }
        return ReturnResultUtils.returnFail(201, "报名失败！");
    }

    @ApiOperation("添加活动")
    @LoginReqired
    @PostMapping(value = "/insertActivity")
    public ReturnResult uploadHeadPhoto(@RequestParam MultipartFile headPhotoFile, ActivityVo activityVo, HttpServletRequest request) throws IOException {
        if (CollectionUtils.isEmpty(activityService.selectActivityTitle(activityVo.getTitle()))) {
            if (headPhotoFile.isEmpty()) {
                System.out.println("文件未上传");
            } else {
                //获取image/jpeg
                String contentType = headPhotoFile.getContentType();
                if (contentType.startsWith("image")) {
                    //获取Web项目的全路径，图片保存路径
                    String realPath = request.getSession().getServletContext().getRealPath("/") + "upload";
                    String newFileName = new SimpleDateFormat("yyyyMMdd").format(new Date()) + ".jpg";
                    FileUtils.copyInputStreamToFile(headPhotoFile.getInputStream(), new File(realPath, newFileName));
                    //将图片路径插入数据库
                    activityVo.setActivityImgurl("upload/" + newFileName);
                    return ReturnResultUtils.returnSucess(activityService.insertActivity(activityVo));
                }
            }
            return ReturnResultUtils.returnFail(235, "添加失败");
        }
        return ReturnResultUtils.returnFail(888, "已有该活动,请勿重复添加");
    }

}
