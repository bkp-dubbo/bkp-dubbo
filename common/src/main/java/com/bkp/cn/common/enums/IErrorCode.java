package com.bkp.cn.common.enums;

public interface IErrorCode {
    String getErrorCode();
    String getErrorMessage();
}
