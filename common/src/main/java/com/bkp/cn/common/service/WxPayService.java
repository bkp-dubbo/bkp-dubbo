package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Orders;

public interface WxPayService {
    /**
     * 微信付款，生成订单
     *
     * @param id
     * @param tel
     * @return
     */
    String isOrder(Integer id, String tel);

    /**
     * 付款成功后，修改订单状态
     *
     * @param orders
     */
    void WxPaySuccess(Orders orders);
}
