package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.LeftMenu;
import com.bkp.cn.common.vo.LeftMenuVo;

import java.util.List;

public interface LeftMenusService {

    /**
     * 查询所有左菜单栏名
     *
     * @param leftMenuName
     * @return
     */
    List<LeftMenu> selectLeftMenus(String leftMenuName);

    /**
     * 选择查询左侧菜单栏
     *
     * @return
     */
    List<LeftMenu> selectLeftMenus(Integer... ids);

    /**
     * 添加左侧菜单栏
     *
     * @param leftMenuName
     * @return
     */
    boolean insertLeftMenus(String leftMenuName);
}
