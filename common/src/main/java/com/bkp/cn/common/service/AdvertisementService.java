package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Advertisement;
import com.bkp.cn.common.vo.AdvertisementVo;

import java.util.List;

public interface AdvertisementService {

    /**
     * 概率随机查询广告
     *
     * @return
     */
    List<Advertisement> selectAdvWeightRandom();

    /**
     * 添加广告
     *
     * @param advertisementVo
     * @return
     */
    boolean insertAdvertisement(AdvertisementVo advertisementVo);

    /**
     * 修改广告
     *
     * @param advertisementVo
     * @return
     */
    boolean updateAdvertisement(AdvertisementVo advertisementVo);

    /**
     * 删除广告
     *
     * @param advId
     * @return
     */
    boolean delAdvertisement(Integer advId);

    /**
     * 通过id查询广告
     *
     * @param advId
     * @return
     */
    Advertisement selectAdvById(Integer advId);

    /**
     * 通过url判断是否存在广告
     *
     * @param url
     * @return
     */
    List<Advertisement> selectAdvByUrl(String url);

}
