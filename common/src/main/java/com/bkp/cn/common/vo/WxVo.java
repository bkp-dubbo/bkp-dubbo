package com.bkp.cn.common.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/215:51
 */
@Data
public class WxVo implements Serializable {
    private Integer id;
    private String openId;
    private String nickName;
    private Integer sex;
    private String language;
    private String city;
    private String headimgurl;
    private String province;
    private String country;
}
