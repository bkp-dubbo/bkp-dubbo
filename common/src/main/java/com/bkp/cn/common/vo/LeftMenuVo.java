package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class LeftMenuVo implements Serializable {
    @ApiModelProperty(value = "左侧菜单id")
    private Integer id;
    @ApiModelProperty(value = "左侧菜单栏名")
    private String leftMenuName;
}
