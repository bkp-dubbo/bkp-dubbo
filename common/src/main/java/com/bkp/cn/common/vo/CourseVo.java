package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CourseVo implements Serializable {
    @ApiModelProperty(value = "课程id")
    private Integer id;
    @ApiModelProperty(value = "课程名")
    private String courseName;
    @ApiModelProperty(value = "课程价格")
    private Long price;
    @ApiModelProperty(value = "参加课程人数")
    private Integer courseNumber;
}
