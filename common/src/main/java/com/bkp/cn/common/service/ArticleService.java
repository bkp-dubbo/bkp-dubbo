package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Article;
import com.bkp.cn.common.dto.ArticleLike;
import com.bkp.cn.common.vo.ArticleVo;

import java.util.List;
import java.util.Map;

public interface ArticleService {
    /**
     * 模糊查询全部文章以及通过左菜单栏
     *
     * @return
     */
    Map selectArticlesBySearch(String searchStr, Integer leftMenuId, int pageNo, int pageSize);

    /**
     * 查询单个文章内容
     *
     * @return
     */
    Article selectArticleById(Integer id);

    /**
     * 添加文章
     *
     * @param articleVo
     * @return
     */
    boolean insertArticle(ArticleVo articleVo);

    /**
     * 点赞
     *
     * @param userId
     * @param articleId
     * @return
     */
    void like(Integer userId, Integer articleId, Integer count);

    /**
     * 修改文章
     *
     * @param articleVo
     * @return
     */
    boolean updateArticle(ArticleVo articleVo);

    /**
     * 删除文章
     *
     * @param articleId
     * @return
     */
    boolean delArticle(Integer articleId);

}
