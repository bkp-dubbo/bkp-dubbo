package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.util.Date;

@Data
public class CommentVo implements Serializable {
    @ApiModelProperty(value = "评论id")
    private Integer id;
    @ApiModelProperty(value = "评论内容")
    private String content;
    @ApiModelProperty(value = "用户Id")
    private Integer userId;
    @ApiModelProperty(value = "文章Id")
    private Integer articleId;
    @ApiModelProperty(value = "评论时间")
    private Date commentTime;
}
