package com.bkp.cn.common.service;

import com.bkp.cn.common.vo.UserVo;

public interface UserService {

    /**
     * 注册用户
     *
     * @param userVo
     * @return
     */
    boolean register(UserVo userVo);

    /**
     * 通过手机号查询用户
     *
     * @param tel
     * @return
     */
    UserVo selectUserByTel(String tel);
}
