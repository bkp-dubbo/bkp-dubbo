package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Binding;
import com.bkp.cn.common.dto.Wx;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.common.vo.WxVo;

import java.util.List;

public interface WxService {

    /**
     * 添加微信信息
     *
     * @param wxVo
     * @return
     */
    void insertWx(WxVo wxVo);

    /**
     * 绑定手机
     *
     * @param tel
     * @param openId
     * @return
     */
    boolean bindTel(String tel, String openId);

    /**
     * 查询是否绑定手机
     *
     * @param openId
     * @return
     */
    boolean isBindTel(String openId);

    /**
     * 通过openId查询微信信息
     *
     * @param openId
     * @return
     */
    Wx selectWxByOpenId(String openId);
}
