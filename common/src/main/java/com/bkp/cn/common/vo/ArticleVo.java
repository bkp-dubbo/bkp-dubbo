package com.bkp.cn.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class ArticleVo implements Serializable {
    @ApiModelProperty(value = "编号")
    private Integer id;
    @ApiModelProperty(value = "文章标题")
    private String title;
    @ApiModelProperty(value = "文章内容")
    private String content;
    @ApiModelProperty(value = "文章点赞数")
    private Long like;
    @ApiModelProperty(value = "用户编号")
    private Integer userId;
    @ApiModelProperty(value = "左菜单栏编号")
    private Integer leftMenuId;
    @ApiModelProperty(value = "上传时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")//后台到前台时间格式转换
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//前台到后台时间格式转换
    private Date uploadTime;
}
