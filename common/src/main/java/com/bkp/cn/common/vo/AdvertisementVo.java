package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/521:09
 */
@Data
public class AdvertisementVo implements Serializable {
    @ApiModelProperty(value = "广告编号")
    private Integer id;
    @ApiModelProperty(value = "广告路径")
    private String adUrl;
    @ApiModelProperty(value = "广告访问路径")
    private String url;
    @ApiModelProperty(value = "广告权重")
    private Integer weights;
}
