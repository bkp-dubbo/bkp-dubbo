package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SearchVo implements Serializable {
    @ApiModelProperty(value = "当前页", required = true)
    private Integer pageNo;
    @ApiModelProperty(value = "每页条数", required = true)
    private Integer pageSize;
    @ApiModelProperty(value = "标题关键字搜索")
    private String title;
    @ApiModelProperty(value = "城市搜索")
    private String city;
    @ApiModelProperty(value = "时间排序：0为降序，1为升序")
    private Integer timeSorting;
}
