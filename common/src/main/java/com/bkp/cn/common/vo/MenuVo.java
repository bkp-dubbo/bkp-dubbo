package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class MenuVo implements Serializable {
    @ApiModelProperty(value = "菜单栏id")
    private Integer id;
    @ApiModelProperty(value = "菜单栏名字")
    private String menuName;
    @ApiModelProperty(value = "菜单栏跳转地址")
    private String url;
}
