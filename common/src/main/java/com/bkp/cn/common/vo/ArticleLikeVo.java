package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/615:57
 */
@Data
public class ArticleLikeVo implements Serializable {
    @ApiModelProperty(value = "编号")
    private Integer id;
    @ApiModelProperty(value = "文章编号")
    private Integer articleId;
    @ApiModelProperty(value = "用户编号")
    private Integer userId;
    @ApiModelProperty(value = "点赞状态，count值为+1或-1，+1为点赞，-1为取消点赞")
    private Integer count;
}
