package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Course;
import com.bkp.cn.common.dto.CourseMember;
import com.bkp.cn.common.vo.CourseVo;

import java.util.List;
import java.util.Map;

public interface CourseService {
    /**
     * 添加之前查重
     *
     * @param courseName
     * @return
     */
    List<Course> selectByCourseName(String courseName);

    /**
     * 后台添加课程内容
     *
     * @param courseVo
     * @return
     */
    boolean insertCourse(CourseVo courseVo);

    /**
     * 首页课程随机推荐
     *
     * @return
     */
    List<Course> selectCourseRand();

    /**
     * 全部课程+课程名关键字查询
     *
     * @param pageNo
     * @param pageSize
     * @param selectStr
     * @return
     */
    Map selectCourse(int pageNo, int pageSize, String selectStr);

    /**
     * 单个课程详情
     *
     * @param id
     * @return
     */
    Course selectCourseById(Integer id);


    /**
     * 查询个人的课程
     *
     * @param tel
     * @return
     */
    List<CourseMember> selectCourseByUserId(String tel, Integer id);
}
