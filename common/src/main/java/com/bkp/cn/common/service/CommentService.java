package com.bkp.cn.common.service;


import com.bkp.cn.common.dto.Comment;
import com.bkp.cn.common.vo.CommentVo;

import java.util.List;

public interface CommentService {

    /**
     * 添加评论
     *
     * @param commentVo
     * @return
     */
    boolean insertComment(CommentVo commentVo);

    /**
     * 修改评论
     *
     * @param commentVo
     */
    boolean updateComment(CommentVo commentVo);

    /**
     * 删除评论
     *
     * @param id
     */
    boolean deleteComment(Integer id);

    /**
     * 查询评论
     *
     * @param articleId
     * @return
     */
    List<Comment> selectComment(Integer articleId);
}