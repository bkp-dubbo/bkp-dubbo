package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Activity;
import com.bkp.cn.common.vo.SearchVo;
import com.bkp.cn.common.vo.ActivityMemberVo;
import com.bkp.cn.common.vo.ActivityVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface ActivityService {
    /**
     * 首页活动随机推荐
     *
     * @return
     */
    List<Activity> selectActivityRand();

    /**
     * 全部活动+活动标题关键字查询
     *
     * @param searchVo
     * @return
     */
    Map selectActivityBySearch(SearchVo searchVo);

    /**
     * 单个活动详情
     *
     * @param id
     * @return
     */
    Activity selectActivityById(Integer id);

    /**
     * 报名参加活动
     *
     * @param activityMembervo
     * @return
     */
    boolean insertActivityMember(ActivityMemberVo activityMembervo);


    /**
     * 查询活动标题
     *
     * @param title
     * @return
     */
    List<Activity> selectActivityTitle(String title);

    /**
     * 新增活动
     *
     * @param activityVo
     */
    boolean insertActivity(ActivityVo activityVo);


}
