package com.bkp.cn.common.service;


import com.bkp.cn.common.dto.Menu;
import com.bkp.cn.common.vo.MenuVo;

import java.util.List;

public interface MenusService {

    /**
     * 菜单栏查询
     *
     * @return
     */
    List<Menu> selectMenus();

    /**
     * 根据id查询菜单
     *
     * @return
     */
    Menu selectMenuById(Integer id);

}
