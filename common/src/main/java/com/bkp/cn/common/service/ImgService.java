package com.bkp.cn.common.service;

import com.bkp.cn.common.dto.Img;

import java.util.List;

public interface ImgService {
    /**
     * 随机查询三张图片以及内容
     */
    List<Img> selectImgRand();

    /**
     * 通过id查询图片以及内容
     */
    Img selectImgById(Integer id);
}
