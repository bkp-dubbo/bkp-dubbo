package com.bkp.cn.common.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ActivityMemberVo implements Serializable {
    @ApiModelProperty(value = "编号")
    private Integer id;
    @ApiModelProperty(value = "用户编号")
    private Integer userId;
    @ApiModelProperty(value = "文章编号")
    private Integer activityId;
}
