package com.bkp.cn.common.dto;

import java.io.Serializable;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table binding
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class Binding implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column binding.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   手机号
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column binding.tel
     *
     * @mbg.generated
     */
    private String tel;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column binding.open_id
     *
     * @mbg.generated
     */
    private String openId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column binding.id
     *
     * @return the value of binding.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column binding.id
     *
     * @param id the value for binding.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column binding.tel
     *
     * @return the value of binding.tel
     *
     * @mbg.generated
     */
    public String getTel() {
        return tel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column binding.tel
     *
     * @param tel the value for binding.tel
     *
     * @mbg.generated
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column binding.open_id
     *
     * @return the value of binding.open_id
     *
     * @mbg.generated
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column binding.open_id
     *
     * @param openId the value for binding.open_id
     *
     * @mbg.generated
     */
    public void setOpenId(String openId) {
        this.openId = openId;
    }
}