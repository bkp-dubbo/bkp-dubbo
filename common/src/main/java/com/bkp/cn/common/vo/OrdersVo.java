package com.bkp.cn.common.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrdersVo implements Serializable {
    private String orderId;
    private Integer courseId;
    private Long orderPrice;
    private String tel;
    private Boolean status;
    private Date createTime;
    private String orderName;
}
