package com.bkp.cn.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class ActivityVo implements Serializable {
    @ApiModelProperty(value = "活动id")
    private Integer id;
    @ApiModelProperty(value = "活动标题")
    private String title;
    @ApiModelProperty(value = "活动内容")
    private String content;
    @ApiModelProperty(value = "活动组织者")
    private String organizer;
    @ApiModelProperty(value = "活动城市")
    private String city;
    @ApiModelProperty(value = "活动地点")
    private String address;
    @ApiModelProperty(value = "活动人数")
    private Integer activityNumber;
    @ApiModelProperty(value = "活动图片")
    private String activityImgurl;
    @ApiModelProperty(value = "活动开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")//后台到前台时间格式转换
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//前台到后台时间格式转换
    private Date startTime;
    @ApiModelProperty(value = "活动结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")//后台到前台时间格式转换
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")//前台到后台时间格式转换
    private Date endTime;
}
