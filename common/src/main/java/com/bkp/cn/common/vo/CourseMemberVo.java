package com.bkp.cn.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CourseMemberVo implements Serializable {
    private Integer id;
    private Integer userId;
    private Integer courseId;
    private CourseVo courseVo;
}
