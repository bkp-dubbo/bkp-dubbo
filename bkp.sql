/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50045
Source Host           : localhost:3306
Source Database       : bkp

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2020-05-09 12:18:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(20) NOT NULL COMMENT '活动标题',
  `content` varchar(255) NOT NULL COMMENT '活动内容',
  `organizer` varchar(11) NOT NULL COMMENT '组织者',
  `city` varchar(10) NOT NULL COMMENT '活动城市',
  `address` varchar(30) NOT NULL COMMENT '举办地点',
  `activity_number` int(11) NOT NULL default '0' COMMENT '活动人数',
  `activity_imgUrl` varchar(50) default NULL COMMENT '活动图片',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '活动结束时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', '111', '沸腾鱼', '啧啧啧', '烦烦烦', '灌灌灌灌', '4', '1111', '2020-05-08 06:00:00', '2020-05-14 00:00:12');
INSERT INTO `activity` VALUES ('2', '今天你海勒吗', '蹦迪', '成思佳', '南京', '江宁', '12', 'upload/20200508.jpg', '2020-05-09 07:00:00', '2020-05-18 00:00:00');
INSERT INTO `activity` VALUES ('3', '今天你嗨了吗', '蹦迪', '成思佳', '南京', '江宁', '12', 'upload/20200509.jpg', '2020-05-09 08:00:00', '2020-05-18 00:00:00');
INSERT INTO `activity` VALUES ('4', '今天你嗨了吗gg', '蹦迪', '成思佳', '盐城', '江宁', '12', 'upload/20200509.jpg', '2020-05-19 00:00:00', '2020-05-18 00:00:00');

-- ----------------------------
-- Table structure for activity_member
-- ----------------------------
DROP TABLE IF EXISTS `activity_member`;
CREATE TABLE `activity_member` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL COMMENT '参加用户id',
  `activity_id` int(11) NOT NULL COMMENT '参加活动id',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity_member
-- ----------------------------
INSERT INTO `activity_member` VALUES ('1', '8', '1');
INSERT INTO `activity_member` VALUES ('2', '8', '1');
INSERT INTO `activity_member` VALUES ('3', '10', '15');

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL auto_increment,
  `ad_url` varchar(50) NOT NULL COMMENT '广告图片地址',
  `url` varchar(50) NOT NULL COMMENT '访问地址',
  `weights` int(2) NOT NULL COMMENT '权重',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advertisement
-- ----------------------------
INSERT INTO `advertisement` VALUES ('2', 'https://mofiter.com/', 'https://mofiter.com/', '3');
INSERT INTO `advertisement` VALUES ('3', 'https://docs.apipost.cn/', 'https://docs.apipost.cn/', '23');
INSERT INTO `advertisement` VALUES ('4', 'https://www.ucloud.cn/', 'https://www.ucloud.cn/', '2');
INSERT INTO `advertisement` VALUES ('5', 'https://www.yisu.com/', 'https://www.yisu.com/', '7');
INSERT INTO `advertisement` VALUES ('6', 'https://www.huaweicloud.com/', 'https://www.huaweicloud.com/', '1');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(20) NOT NULL COMMENT '文章标题',
  `content` longtext NOT NULL COMMENT '文章内容',
  `like` bigint(20) default NULL,
  `user_id` int(11) NOT NULL COMMENT '用户',
  `left_menu_id` int(11) NOT NULL COMMENT '类型',
  `upload_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '上传时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('3', 'weisg', '为什么又有这么多为什么', '10', '10', '3', '2020-05-09 11:40:02');
INSERT INTO `article` VALUES ('5', '十万个为什么', '为什么这么多问题', '3', '8', '4', '2020-05-09 09:02:05');

-- ----------------------------
-- Table structure for article_like
-- ----------------------------
DROP TABLE IF EXISTS `article_like`;
CREATE TABLE `article_like` (
  `id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `count` int(11) NOT NULL COMMENT '点赞数量',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article_like
-- ----------------------------

-- ----------------------------
-- Table structure for binding
-- ----------------------------
DROP TABLE IF EXISTS `binding`;
CREATE TABLE `binding` (
  `id` int(11) NOT NULL auto_increment,
  `tel` varchar(11) NOT NULL COMMENT '手机号',
  `open_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of binding
-- ----------------------------
INSERT INTO `binding` VALUES ('7', '18861999986', 'o-Q1swjaMBjYz2hwAFA8S5MH4QEs');
INSERT INTO `binding` VALUES ('8', '18209485507', 'o-Q1swswfemle_bRsCSCzxY791cs');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL auto_increment,
  `content` varchar(150) NOT NULL COMMENT '评论内容',
  `user_id` int(11) NOT NULL COMMENT '用户',
  `article_id` int(11) NOT NULL COMMENT '文章',
  `comment_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '评论时间',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('3', '嘻嘻嘻嘻嘻', '8', '3', '2020-05-08 16:35:44');
INSERT INTO `comment` VALUES ('5', '嘻嘻嘻嘻嘻', '10', '3', '2020-05-09 11:29:06');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course` (
  `id` int(11) NOT NULL auto_increment,
  `course_name` varchar(255) NOT NULL COMMENT '课程名',
  `price` decimal(10,0) NOT NULL COMMENT '课程价格',
  `course_number` int(11) NOT NULL default '0' COMMENT '课程参加人数',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('11', 'Java开发', '1', '2');
INSERT INTO `course` VALUES ('12', 'Java后端开发【名师-薛其俊】', '1', '1000');
INSERT INTO `course` VALUES ('13', 'Java开发1', '1', '1');

-- ----------------------------
-- Table structure for course_member
-- ----------------------------
DROP TABLE IF EXISTS `course_member`;
CREATE TABLE `course_member` (
  `id` int(11) NOT NULL auto_increment,
  `user_tel` varchar(11) NOT NULL COMMENT '用户名',
  `course_id` int(11) NOT NULL COMMENT '课程',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of course_member
-- ----------------------------
INSERT INTO `course_member` VALUES ('10', '15605190015', '11');
INSERT INTO `course_member` VALUES ('11', '18209485507', '13');

-- ----------------------------
-- Table structure for img
-- ----------------------------
DROP TABLE IF EXISTS `img`;
CREATE TABLE `img` (
  `id` int(11) NOT NULL auto_increment,
  `img_url` varchar(50) NOT NULL COMMENT '图片地址',
  `url` varchar(50) NOT NULL COMMENT '访问地址',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of img
-- ----------------------------
INSERT INTO `img` VALUES ('1', '/img/Goods/Aiviy-iobit-asc-box-418x418.jpg', '/img/Goods/Aiviy-iobit-asc-box-418x418.jpg');
INSERT INTO `img` VALUES ('2', '/img/Goods/Aiviy-iobit-ascu-img01-418x418.jpg', '/img/Goods/Aiviy-iobit-ascu-img01-418x418.jpg');
INSERT INTO `img` VALUES ('3', '/img/Goods/Good2-1.jpg', '/img/Goods/Good2-1.jpg');
INSERT INTO `img` VALUES ('4', '/img/Goods/Good2-2.png', '/img/Goods/Good2-2.png');
INSERT INTO `img` VALUES ('5', '/img/Goods/Good3-1.jpg', '/img/Goods/Good3-1.jpg');
INSERT INTO `img` VALUES ('6', '/img/Goods/Good3-2.jpg', '/img/Goods/Good3-2.jpg');
INSERT INTO `img` VALUES ('7', '/img/Goods/Good4-1.jpg', '/img/Goods/Good4-1.jpg');

-- ----------------------------
-- Table structure for left_menu
-- ----------------------------
DROP TABLE IF EXISTS `left_menu`;
CREATE TABLE `left_menu` (
  `id` int(4) NOT NULL auto_increment,
  `left_menu_name` varchar(10) NOT NULL COMMENT '左边栏名',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of left_menu
-- ----------------------------
INSERT INTO `left_menu` VALUES ('1', '前端');
INSERT INTO `left_menu` VALUES ('2', '后端');
INSERT INTO `left_menu` VALUES ('3', '小程序');
INSERT INTO `left_menu` VALUES ('4', '云计算');
INSERT INTO `left_menu` VALUES ('5', '人工智能');
INSERT INTO `left_menu` VALUES ('6', 'SpringBoot');
INSERT INTO `left_menu` VALUES ('7', 'dubbo');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(3) NOT NULL auto_increment,
  `menu_name` varchar(10) NOT NULL COMMENT '菜单栏名',
  `url` varchar(50) NOT NULL COMMENT '菜单栏地址',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '首页', 'https://segmentfault.com/');
INSERT INTO `menu` VALUES ('2', '问答', 'https://segmentfault.com/questions');
INSERT INTO `menu` VALUES ('3', '专栏', 'https://segmentfault.com/blogs');
INSERT INTO `menu` VALUES ('4', '资讯', 'https://segmentfault.com/news');
INSERT INTO `menu` VALUES ('5', '课程', 'https://segmentfault.com/lives');
INSERT INTO `menu` VALUES ('6', '活动', 'https://segmentfault.com/events');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` varchar(40) NOT NULL COMMENT '订单编号',
  `course_id` int(11) NOT NULL COMMENT '课程',
  `order_price` decimal(10,0) NOT NULL,
  `tel` varchar(11) NOT NULL COMMENT '用户手机号',
  `status` tinyint(1) NOT NULL default '0' COMMENT '状态（1：已支付，0未支付）',
  `create_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY  (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('342891837', '11', '1', '18861999986', '0', '2020-05-09 10:37:25');
INSERT INTO `orders` VALUES ('513169501', '13', '1', '18209485507', '1', '2020-05-09 11:54:08');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(11) default NULL COMMENT '用户名',
  `tel` varchar(11) NOT NULL COMMENT '手机号',
  `password` varchar(50) default NULL COMMENT '密码',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('8', '刘长军', '18861999986', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `user` VALUES ('9', '陈森富', '18014766869', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `user` VALUES ('10', null, '18209485507', null);

-- ----------------------------
-- Table structure for wx
-- ----------------------------
DROP TABLE IF EXISTS `wx`;
CREATE TABLE `wx` (
  `id` int(11) NOT NULL auto_increment,
  `open_id` varchar(50) NOT NULL COMMENT '微信唯一标识',
  `nick_name` varchar(11) default NULL COMMENT '昵称',
  `sex` int(1) default NULL COMMENT '性别(1:男，0：女)',
  `language` varchar(10) default NULL COMMENT '语言',
  `city` varchar(10) default NULL COMMENT '城市',
  `headimgurl` varchar(255) default NULL COMMENT '头像链接',
  `province` varchar(10) default NULL COMMENT '省',
  `country` varchar(10) default NULL COMMENT '国家',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wx
-- ----------------------------
INSERT INTO `wx` VALUES ('4', 'o-Q1swswfemle_bRsCSCzxY791cs', '布丁', '2', 'zh_CN', '南京', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJbweGEukIcYovPtHPexdH8bE6cv9nNibViaRsZrF0BYeK67JyxERmZyzA1hicsFyicx1orBlHVtmfQ4A/132', '江苏', '中国');
INSERT INTO `wx` VALUES ('5', 'o-Q1swjaMBjYz2hwAFA8S5MH4QEs', 'Springboot', '1', 'zh_CN', '盐城', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLsLAV4cARu9JaNNyTmv3X57AicWjlZHmcFDqwWm0Afcm95La8icI3YLeNtH3e65IwWruq7TnicBEvDQ/132', '江苏', '中国');
INSERT INTO `wx` VALUES ('6', 'o-Q1swjyYp0v1GDHtp5Fxbsag6jQ', 'CHEN', '1', 'zh_CN', '无锡', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKicOZVq2sKuVLpflk3sZa3RRFiaJVpQSicX7icA3yCibQibqNo7tQnYKRQGKWLticlxeMv0ibShFicia1JTjibQ/132', '江苏', '中国');
