package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.*;
import com.bkp.cn.common.enums.ErrorEnums;
import com.bkp.cn.common.exception.ServiceException;
import com.bkp.cn.common.service.ArticleService;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.vo.ArticleVo;
import com.bkp.cn.provider.mapper.ArticleLikeMapper;
import com.bkp.cn.provider.mapper.ArticleMapper;
import com.bkp.cn.provider.mapper.CommentMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private ArticleLikeMapper articleLikeMapper;
    private Jedis redisOps = new Jedis("localhost", 6379);
    String article_like = "article_like:";
    String article_like_sum = "article_like_sum:";
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public Map selectArticlesBySearch(String searchStr, Integer leftMenuId, int pageNo, int pageSize) {
        ArticleExample articleExample = new ArticleExample();
        articleExample.setLimit(pageNo);
        articleExample.setOffset(pageSize);
        ArticleExample.Criteria criteria = articleExample.createCriteria();
        if (StringUtils.isNotEmpty(searchStr)) {
            criteria.andTitleLike("%" + searchStr + "%");
        }
        if (null != leftMenuId) {
            criteria.andLeftMenuIdEqualTo(leftMenuId);
        }
        articleExample.setOrderByClause("`like` desc");
        List<Article> articleList = articleMapper.selectByExample(articleExample);
        long count = articleMapper.countByExample(articleExample);
        Map<String, Object> map = Maps.newHashMap();
        map.put("currentList", articleList);
        map.put("totalCount", count);
        return map;
    }

    @Override
    public Article selectArticleById(Integer id) {
        ArticleExample articleExample = new ArticleExample();
        articleExample.createCriteria().andIdEqualTo(id);
        Article article = articleMapper.selectArticleById(id);
        return article;
    }

    @Override
    public boolean insertArticle(ArticleVo articleVo) {
        Article article = new Article();
        BeanUtils.copyProperties(articleVo, article);
        return articleMapper.insertSelective(article) > 0;
    }

    @Override
    public void like(Integer userId, Integer articleId, Integer count) {
        if (isLike(userId, articleId) && count == +1) {
            throw new ServiceException(ErrorEnums.ERROR_LIKE);
        }
        redisOps.lpush(article_like + articleId + ":" + userId, String.valueOf(count));
        //计算总数
        if (count == +1) {
            redisOps.incrBy(article_like_sum + articleId, 1);
        } else {
            redisOps.decrBy(article_like_sum + articleId, 1);
        }
    }

    @Override
    public boolean updateArticle(ArticleVo articleVo) {
        Article article = new Article();
        BeanUtils.copyProperties(articleVo, article);
        return articleMapper.updateByPrimaryKeySelective(article) > 0;
    }

    @Override
    public boolean delArticle(Integer articleId) {
        CommentExample commentExample = new CommentExample();
        commentExample.createCriteria().andArticleIdEqualTo(articleId);
        ArticleLikeExample articleLikeExample = new ArticleLikeExample();
        articleLikeExample.createCriteria().andArticleIdEqualTo(articleId);
        List<Comment> commentList = commentMapper.selectByExample(commentExample);
        List<ArticleLike> articleLikeList = articleLikeMapper.selectByExample(articleLikeExample);
        if (CollectionUtils.isNotEmpty(commentList)) {
            commentMapper.deleteByArticleId(articleId);
        }
        if (CollectionUtils.isNotEmpty(articleLikeList)) {
            articleLikeMapper.deleteByArticleId(articleId);
        }
        return articleMapper.deleteByPrimaryKey(articleId) > 0;
    }

    /**
     * 是否点赞
     *
     * @param userId
     * @param articleId
     * @return
     */
    public boolean isLike(Integer userId, Integer articleId) {
        //某商品某用户的点赞记录
        List<String> countList = redisOps.lrange(article_like + articleId + ":" + userId, 0, -1);
        List<Integer> integerList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(countList)) {
            countList.stream().forEach(s -> {
                integerList.add(Integer.valueOf(s));
            });
            if (integerList.stream().mapToInt(i -> i).sum() > 0) {
                return true;
            }
        } else {
            ArticleLikeExample articleLikeExample = new ArticleLikeExample();
            articleLikeExample.createCriteria().andArticleIdEqualTo(articleId).andUserIdEqualTo(userId);
            List<ArticleLike> articleLikeList = articleLikeMapper.selectByExample(articleLikeExample);
            if (CollectionUtils.isNotEmpty(articleLikeList)) {
                return articleLikeList.stream().mapToInt(ArticleLike::getCount).sum() > 0;
            }
            return false;
        }
        return false;
    }

    @Scheduled(cron = "0 0 1 * * ?")
    public void insertArticleLike() {
        articleLikeMapper.truncateTable();
        Set<String> keys = redisOps.keys("*" + article_like + "*");
        keys.stream().forEach(key -> {
            String[] articleIdAndUserIds = key.split(":");
            ArticleLike articleLike = new ArticleLike();
            articleLike.setArticleId(Integer.valueOf(articleIdAndUserIds[0]));
            articleLike.setUserId(Integer.valueOf(articleIdAndUserIds[1]));
            //某商品某用户的点赞记录
            List<String> countList = redisOps.lrange(key, 0, -1);
            List<Integer> integerList = Lists.newArrayList();
            countList.stream().forEach(s -> {
                integerList.add(Integer.valueOf(s));
            });
            articleLike.setCount(integerList.stream().mapToInt(i -> i).sum());
            articleLikeMapper.insertSelective(articleLike);
            String likeStr = redisOps.get(article_like_sum + articleLike.getArticleId());
            //更新文章点赞数
            articleMapper.updateLike(Long.valueOf(likeStr), articleLike.getArticleId());
        });

    }

}
