package com.bkp.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.Menu;

import com.bkp.cn.common.dto.MenuExample;
import com.bkp.cn.common.service.MenusService;
import com.bkp.cn.common.vo.MenuVo;
import com.bkp.cn.provider.mapper.MenuMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Service
public class MenuServiceImpl implements MenusService {

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> selectMenus() {
        return menuMapper.selectByExample(null);
    }

    @Override
    public Menu selectMenuById(Integer id) {
        return menuMapper.selectByPrimaryKey(id);
    }

}
