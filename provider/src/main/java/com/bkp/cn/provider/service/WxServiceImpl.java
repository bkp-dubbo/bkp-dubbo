package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.dto.*;
import com.bkp.cn.common.service.WxService;
import com.bkp.cn.common.utils.RedisUtils;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.common.vo.WxVo;
import com.bkp.cn.provider.mapper.BindingMapper;
import com.bkp.cn.provider.mapper.UserMapper;
import com.bkp.cn.provider.mapper.WxMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/4/616:47
 */
@Service
@Transactional
public class WxServiceImpl implements WxService {
    @Autowired
    private WxMapper wxMapper;
    @Autowired
    private BindingMapper bindingMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    @JmsListener(destination = "wxLogin.wxVo")
    public void insertWx(WxVo wxVo) {
        if (ObjectUtils.isEmpty(selectWxByOpenId(wxVo.getOpenId()))) {
            Wx wx = new Wx();
            BeanUtils.copyProperties(wxVo, wx);
            wxMapper.insertSelective(wx);
        }
    }

    @Override
    public boolean bindTel(String tel, String openId) {
        Binding binding = new Binding();
        binding.setTel(tel);
        binding.setOpenId(openId);
        User user = new User();
        user.setTel(tel);
        UserExample userExample = new UserExample();
        userExample.createCriteria().andTelEqualTo(tel);
        List<User> userList = userMapper.selectByExample(userExample);
        if (CollectionUtils.isEmpty(userList)) {
            userMapper.insertSelective(user);
        }
        return bindingMapper.insertSelective(binding) > 0;
    }

    @Override
    public boolean isBindTel(String openId) {
        BindingExample bindingExample = new BindingExample();
        bindingExample.createCriteria().andOpenIdEqualTo(openId);
        List<Binding> bindingList = bindingMapper.selectByExample(bindingExample);
        if (CollectionUtils.isNotEmpty(bindingList)) {
            return true;
        }
        return false;
    }

    @Override
    public Wx selectWxByOpenId(String openId) {
        WxExample wxExample = new WxExample();
        wxExample.createCriteria().andOpenIdEqualTo(openId);
        List<Wx> wxList = wxMapper.selectByExample(wxExample);
        if (CollectionUtils.isNotEmpty(wxList)) return wxList.get(0);

        return null;
    }
}
