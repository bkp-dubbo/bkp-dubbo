package com.bkp.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.Article;
import com.bkp.cn.common.dto.Comment;
import com.bkp.cn.common.dto.CommentExample;
import com.bkp.cn.common.service.CommentService;
import com.bkp.cn.common.vo.CommentVo;
import com.bkp.cn.provider.mapper.ArticleMapper;
import com.bkp.cn.provider.mapper.CommentMapper;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public boolean insertComment(CommentVo commentVo) {
        Comment comment = new Comment();
        BeanUtils.copyProperties(commentVo, comment);
        return commentMapper.insertSelective(comment) > 0;
    }

    @Override
    public boolean updateComment(CommentVo commentVo) {
        Comment comment = new Comment();
        BeanUtils.copyProperties(commentVo, comment);
        return commentMapper.updateByPrimaryKeySelective(comment) > 0;
    }

    @Override
    public boolean deleteComment(Integer id) {
        return commentMapper.deleteByPrimaryKey(id) > 0;

    }

    @Override
    public List<Comment> selectComment(Integer articleId) {
        CommentExample commentExample = new CommentExample();
        commentExample.createCriteria().andArticleIdEqualTo(articleId);
        //通过时间排序
        commentExample.setOrderByClause("comment_time desc");
        return commentMapper.selectByArticleId(commentExample);
    }
}
