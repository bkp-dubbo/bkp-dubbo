package com.bkp.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.Img;
import com.bkp.cn.common.dto.ImgExample;
import com.bkp.cn.common.service.ImgService;
import com.bkp.cn.provider.mapper.ImgMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class ImgServiceImpl implements ImgService {
    @Autowired
    private ImgMapper imgMapper;

    @Override
    public List<Img> selectImgRand() {
        return imgMapper.selectImgRand();
    }

    @Override
    public Img selectImgById(Integer id) {
        ImgExample imgExample = new ImgExample();
        imgExample.createCriteria().andIdEqualTo(id);
        Img img = imgMapper.selectByPrimaryKey(id);
        return img;
    }
}
