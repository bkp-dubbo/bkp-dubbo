package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.*;
import com.bkp.cn.common.service.CourseService;
import com.bkp.cn.common.vo.CourseVo;
import com.bkp.cn.provider.mapper.CourseMapper;
import com.bkp.cn.provider.mapper.CourseMemberMapper;
import com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private CourseMemberMapper courseMemberMapper;

    @Override
    public List<Course> selectByCourseName(String courseName) {
        CourseExample courseExample = new CourseExample();
        courseExample.createCriteria().andCourseNameEqualTo(courseName);
        return courseMapper.selectByCourseName(courseExample);
    }

    @Override
    public boolean insertCourse(CourseVo courseVo) {
        Course courseDto = new Course();
        BeanUtils.copyProperties(courseVo, courseDto);
        return courseMapper.insertSelective(courseDto) > 0;
    }

    @Override
    public List<Course> selectCourseRand() {
        return courseMapper.selectCourseRand(3);
    }

    @Override
    public Map selectCourse(int pageNo, int pageSize, String selectStr) {
        CourseExample courseExample = new CourseExample();
        Map<String, Object> maps = Maps.newHashMap();
        courseExample.setLimit(pageNo);
        courseExample.setOffset(pageSize);
        if (StringUtils.isNotEmpty(selectStr)) courseExample.createCriteria().andCourseNameLike('%' + selectStr + '%');

        List<Course> courseList = courseMapper.selectByExample(courseExample);
        maps.put("currentList", courseList);
        maps.put("totalCount", courseMapper.countByExample(courseExample));
        return maps;
    }

    @Override
    public Course selectCourseById(Integer id) {
        return courseMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CourseMember> selectCourseByUserId(String tel, Integer id) {
        List<CourseMember> courseMemberList = courseMemberMapper.selectByExample(tel, id);
        return courseMemberList;
    }
}
