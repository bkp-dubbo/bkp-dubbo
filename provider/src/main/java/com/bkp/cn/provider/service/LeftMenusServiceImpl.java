package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.LeftMenu;
import com.bkp.cn.common.dto.LeftMenuExample;
import com.bkp.cn.common.dto.Menu;
import com.bkp.cn.common.service.LeftMenusService;
import com.bkp.cn.common.vo.LeftMenuVo;
import com.bkp.cn.provider.mapper.LeftMenuMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

@Service
public class LeftMenusServiceImpl implements LeftMenusService {

    @Autowired
    private LeftMenuMapper leftMenuMapper;

    @Override
    public List<LeftMenu> selectLeftMenus(String leftMenuName) {
        LeftMenuExample leftMenuExample = new LeftMenuExample();
        leftMenuExample.createCriteria().andLeftMenuNameEqualTo(leftMenuName);
        return leftMenuMapper.selectByExample(leftMenuExample);
    }

    @Override
    public List<LeftMenu> selectLeftMenus(Integer... ids) {
        LeftMenuExample leftMenuExample = new LeftMenuExample();
        List<Integer> idList = Arrays.asList(ids);
        leftMenuExample.createCriteria().andIdIn(idList);
        return leftMenuMapper.selectByExample(leftMenuExample);
    }

    @Override
    public boolean insertLeftMenus(String leftMenuName) {
        LeftMenu leftMenu = new LeftMenu();
        leftMenu.setLeftMenuName(leftMenuName);
        return leftMenuMapper.insertSelective(leftMenu) > 0;
    }
}
