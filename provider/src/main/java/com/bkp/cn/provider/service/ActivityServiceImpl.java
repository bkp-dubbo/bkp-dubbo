package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.Activity;
import com.bkp.cn.common.dto.ActivityExample;
import com.bkp.cn.common.dto.ActivityMember;
import com.bkp.cn.common.service.ActivityService;
import com.bkp.cn.common.vo.SearchVo;
import com.bkp.cn.common.vo.ActivityMemberVo;
import com.bkp.cn.common.vo.ActivityVo;
import com.bkp.cn.provider.mapper.ActivityMapper;
import com.bkp.cn.provider.mapper.ActivityMemberMapper;
import com.google.common.collect.Maps;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityMemberMapper activityMemberMapper;

    @Override
    public List<Activity> selectActivityRand() {
        return activityMapper.selectActivityRand(3);
    }

    @Override
    public Map selectActivityBySearch(SearchVo searchVo) {
        ActivityExample activityExample = new ActivityExample();
        Map<String, Object> maps = Maps.newHashMap();
        activityExample.setLimit(searchVo.getPageNo());
        activityExample.setOffset(searchVo.getPageSize());
        List<Activity> activityList = activityMapper.selectActivity(searchVo, activityExample);
        maps.put("activityList", activityList);
        maps.put("totalCount", activityMapper.countByExample(activityExample));
        return maps;
    }

    @Override
    public Activity selectActivityById(Integer id) {
        return activityMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean insertActivityMember(ActivityMemberVo activityMemberVo) {
        ActivityMember activityMember = new ActivityMember();
        BeanUtils.copyProperties(activityMemberVo, activityMember);
        activityMapper.updateActivity(activityMember.getActivityId());
        return activityMemberMapper.insertSelective(activityMember) > 0;
    }

    @Override
    public List<Activity> selectActivityTitle(String title) {
        ActivityExample activityExample = new ActivityExample();
        activityExample.createCriteria().andTitleEqualTo(title);
        return activityMapper.selectByTitle(activityExample);
    }

    @Override
    public boolean insertActivity(ActivityVo activityVo) {
        Activity activity = new Activity();
        BeanUtils.copyProperties(activityVo, activity);
        return activityMapper.insertSelective(activity) > 0;
    }

}
