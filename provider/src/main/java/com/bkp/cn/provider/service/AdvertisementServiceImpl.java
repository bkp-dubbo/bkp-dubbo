package com.bkp.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.Advertisement;
import com.bkp.cn.common.dto.AdvertisementExample;
import com.bkp.cn.common.service.AdvertisementService;
import com.bkp.cn.common.vo.AdvertisementVo;
import com.bkp.cn.provider.mapper.AdvertisementMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/5/510:47
 */
@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {
    @Autowired
    private AdvertisementMapper advertisementMapper;

    @Override
    public List<Advertisement> selectAdvWeightRandom() {
        AdvertisementExample advertisementExample = new AdvertisementExample();
        List<Advertisement> advertisementList = advertisementMapper.selectByExample(advertisementExample);
        List<Advertisement> advList = Lists.newArrayList();
        Set<Integer> countSet = Sets.newHashSet();
        while (true) {
            Advertisement adv = getAdvertisement(advertisementList);
            if (countSet.add(adv.getId())) {
                advList.add(adv);
            }
            if (countSet.size() == 3) {
                break;
            }
        }
        return advList;
    }

    @Override
    public boolean insertAdvertisement(AdvertisementVo advertisementVo) {
        Advertisement advertisement = new Advertisement();
        BeanUtils.copyProperties(advertisementVo, advertisement);
        return advertisementMapper.insertSelective(advertisement) > 0;
    }

    @Override
    public boolean updateAdvertisement(AdvertisementVo advertisementVo) {
        Advertisement advertisement = new Advertisement();
        BeanUtils.copyProperties(advertisementVo, advertisement);
        return advertisementMapper.updateByPrimaryKeySelective(advertisement) > 0;
    }

    @Override
    public boolean delAdvertisement(Integer advId) {
        return advertisementMapper.deleteByPrimaryKey(advId) > 0;
    }

    @Override
    public Advertisement selectAdvById(Integer advId) {
        return advertisementMapper.selectByPrimaryKey(advId);
    }

    @Override
    public List<Advertisement> selectAdvByUrl(String url) {
        AdvertisementExample advertisementExample = new AdvertisementExample();
        advertisementExample.createCriteria().andUrlEqualTo(url);
        List<Advertisement> advertisementList = advertisementMapper.selectByExample(advertisementExample);
        return advertisementList;
    }

    /**
     * 通过权重获取对象
     *
     * @param lists
     * @return
     */
    public Advertisement getAdvertisement(List<Advertisement> lists) {
        //计算权重之和
        Integer weightSum = 0;
        for (Advertisement advertisement : lists) {
            weightSum += advertisement.getWeights();
        }
        if (weightSum <= 0) {
            return null;
        }
        Integer randomNum = Integer.valueOf((int) Math.floor(Math.random() * weightSum));
        Integer m = 0;
        Advertisement adv = null;
        for (Advertisement advertisement : lists) {
            if (m <= randomNum && randomNum < m + advertisement.getWeights()) {
                adv = advertisement;
                break;
            }
            m += advertisement.getWeights();
        }
        return adv;
    }
}
