package com.bkp.cn.provider.service;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Service;
import com.bkp.cn.common.dto.User;
import com.bkp.cn.common.dto.UserExample;
import com.bkp.cn.common.service.UserService;
import com.bkp.cn.common.utils.SHAUtils;
import com.bkp.cn.common.vo.UserVo;
import com.bkp.cn.provider.mapper.UserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * @Description TODO
 * @Author chensijia
 * @Date 2020/4/3010:20
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean register(UserVo userVo) {
        User user = new User();
        BeanUtils.copyProperties(userVo, user);
        //密码SHA加密
        user.setPassword(SHAUtils.stringSha1(user.getPassword()));
        return userMapper.insertSelective(user) > 0;
    }

    @Override
    public UserVo selectUserByTel(String tel) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andTelEqualTo(tel);
        List<User> userList = userMapper.selectByExample(userExample);
        if (CollectionUtils.isNotEmpty(userList)) {
            User user = userList.get(0);
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(user, userVo);
            return userVo;
        }
        return null;
    }
}
