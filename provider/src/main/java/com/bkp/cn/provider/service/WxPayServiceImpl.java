package com.bkp.cn.provider.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.bkp.cn.common.conf.WxPayConfig;
import com.bkp.cn.common.dto.Course;
import com.bkp.cn.common.dto.CourseMember;
import com.bkp.cn.common.dto.Orders;
import com.bkp.cn.common.dto.OrdersExample;
import com.bkp.cn.common.service.WxPayService;
import com.bkp.cn.common.utils.*;
import com.bkp.cn.common.vo.OrdersVo;
import com.bkp.cn.provider.mapper.CourseMapper;
import com.bkp.cn.provider.mapper.CourseMemberMapper;
import com.bkp.cn.provider.mapper.OrdersMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@Transactional
public class WxPayServiceImpl implements WxPayService {
    @Autowired
    private WxPayConfig wxPayConfig;
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private CourseMemberMapper courseMemberMapper;
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public String isOrder(Integer id, String tel) {
        OrdersVo ordersVo = new OrdersVo();
        Course course = courseMapper.selectByPrimaryKey(id);
        int num = Math.abs(new Random().nextInt());
        ordersVo.setOrderId(String.valueOf(num));
        ordersVo.setCourseId(course.getId());
        ordersVo.setTel(tel);
        ordersVo.setOrderName(course.getCourseName());
        ordersVo.setOrderPrice(course.getPrice());
        insertOrder(ordersVo);
        try {
            return unifiedOrder(ordersVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 微信统一下单
     *
     * @param ordersVo
     * @return
     * @throws Exception
     */
    private String unifiedOrder(OrdersVo ordersVo) {
        SortedMap<String, String> param = new TreeMap<>();
        param.put("appid", wxPayConfig.getAppId());
        param.put("mch_id", wxPayConfig.getMchId());
        param.put("nonce_str", CommonUtils.getUUID());
        param.put("body", ordersVo.getOrderName());
        param.put("out_trade_no", ordersVo.getOrderId());
        param.put("total_fee", String.valueOf(ordersVo.getOrderPrice()));
        param.put("spbill_create_ip", "192.168.3.102");
        param.put("notify_url", wxPayConfig.getNotifyUrl());
        param.put("trade_type", "NATIVE");

        param.put("sign", WxPayUtils.generateSignature(param, wxPayConfig.getKey()));
        String payXml = WxPayUtils.mapToXml(param);
        //请求统一下单接口
        String order = HttpClientUtils.doPost(wxPayConfig.getUnifiedUrl(), payXml, 4000);
        Map<String, String> resultMap = WxPayUtils.xmlToMap(order);
        if ("SUCCESS".equals(resultMap.get("return_code")) && "SUCCESS".equals(resultMap.get("result_code"))) {
            String codeUrl = resultMap.get("code_url");
            if (null != codeUrl) {
                return codeUrl;
            }
        }
        return null;
    }

    /**
     * 数据库异步插入订单
     *
     * @param ordersVo
     */
    @Async
    public void insertOrder(OrdersVo ordersVo) {
        OrdersExample ordersExample = new OrdersExample();
        ordersExample.createCriteria().andCourseIdEqualTo(ordersVo.getCourseId()).andTelEqualTo(ordersVo.getTel());
        List<Orders> ordersList = ordersMapper.selectByExample(ordersExample);
        if (CollectionUtils.isEmpty(ordersList)) {
            Orders orders = new Orders();
            BeanUtils.copyProperties(ordersVo, orders);
            String jsonStr = JSONObject.toJSONString(orders);
            String nameSpace = "COURSE_ORDERS:";
            redisUtils.set(nameSpace + orders.getOrderId(), jsonStr);
            ordersMapper.insertSelective(orders);
        }
    }

    @Override
    public void WxPaySuccess(Orders orders) {
        CourseMember courseMember = new CourseMember();
        courseMember.setCourseId(orders.getCourseId());
        courseMember.setUserTel(orders.getTel());
        courseMemberMapper.insertSelective(courseMember);
        ordersMapper.updateOrderStatus(orders.getOrderId(), true);
        courseMapper.updateCourseNum(orders.getCourseId());
    }
}
